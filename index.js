let http = require('http');

let courses = [
	{
		"course":"HTML",
		"desc": "Hypertext Markup Language"
	},
	{
		"course":"CSS",
		"desc": "Cascade Styling Sheet"
	}
]

http.createServer(function(req,res){
	if(req.url == "/" && req.method == "GET"){
		res.writeHead(200,{"Content-Type":"text/plain"});
		res.end("Welcome to Booking System");
	}

	if(req.url == "/profile" && req.method == "GET"){
		res.writeHead(200,{"Content-Type":"text/plain"});
		res.end("Welcome to your profile");
	}

		if(req.url == "/courses" && req.method == "GET"){
	res.writeHead(200,{"Content-Type":"text/plain"});
	res.end("Here's our courses available");
	}

	// ADD

	if(req.url == "/addcourse" && req.method == "POST"){
		let reqBody = "";
		req.on("data", function(data){
			reqBody += data;
		});
		req.on("end", function(){
			console.log(typeof reqBody);
			reqBody = JSON.parse(reqBody);
			let newCourse = {
				"course":reqBody.course,
				"desc":reqBody.desc
			}		
				courses.push(newCourse);
				console.log(courses);

			res.writeHead(200,{"Content-Type": "application/json"});
			res.write(JSON.stringify(newCourse));
			res.end("Add course to our reources");
			})
	}

	// UPDATE
			if(req.url == "/updateCourse" && req.method == "PUT"){
		let reqBody = "";
		req.on("data", function(data){
			reqBody += data;
		});
		req.on("end", function(){
			console.log(typeof reqBody);
			reqBody = JSON.parse(reqBody);
			let newCourse = {
				"course":reqBody.course,
				"desc":reqBody.desc
			}		
				courses.push(newCourse);
				console.log(courses);

			res.writeHead(200,{"Content-Type": "application/json"});
			res.write(JSON.stringify(newCourse));
			res.end("Update  acourse to our resources");
			})
	}

	if(req.url == "/deleteCourse" && req.method == "DELETE"){
		let reqBody = "";
		req.on("data", function(data){
			reqBody += data;
		});
		req.on("end", function(){
			console.log(typeof reqBody);
			reqBody = JSON.parse(reqBody);
			let newCourse = {
				"course":reqBody.course,
				"desc":reqBody.desc
			}		
				courses.pop(newCourse);
				console.log(courses);

			res.writeHead(200,{"Content-Type": "application/json"});
			res.write(JSON.stringify(newCourse));
			res.end("Archive courses to our resources");
			})
	}

	

}).listen(4000);

console.log("Sytem is saved in local port 4000");

